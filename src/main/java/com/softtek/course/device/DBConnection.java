package com.softtek.course.device;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException; 
public class DBConnection {
	private static Connection conn = null;
	public DBConnection() {
		String url ="jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#";
		String driver = "com.mysql.jdbc.Driver";
		String user = "root";
		String pass =  "1234";
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, pass);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection() {
		if(conn == null) {
			new DBConnection();
		}
		return conn;
	}
}
