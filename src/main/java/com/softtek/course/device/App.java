package com.softtek.course.device;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class App {
	public static void main( String[] args )
    {
		List<Device> result = new ArrayList<>();
	
		String SQL_SELECT = "Select * from DEVICE";
		
		DBConnection conn = new DBConnection();
		PreparedStatement preparedStatement;
		try {
			preparedStatement = conn.getConnection().prepareStatement(SQL_SELECT);

			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int deviceId = resultSet.getInt("deviceId");
			    String name = resultSet.getString("name");
			    String description = resultSet.getString("description");
			    int manufacturerId = resultSet.getInt("manufacturerId");
			    int colorId = resultSet.getInt("colorId");
			    String comments = resultSet.getString("comments");
			    Device device = new Device(deviceId, name, description, manufacturerId, 
			    		colorId, comments);
			    result.add(device);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    for (Device d : result) {
			System.out.println("ID: "+ d.getDeviceId() + " device's name: " 
					+ d.getName() + " Description " + d.getDescription()  
					+ " Manufacturer Id: " + d.getManufacturerId()  
					+ " Color Id: " + d.getColorId() 
					+ " Comments: " + d.getComments());
		}
	    List <String> names = result.stream()
	    .filter(d -> d.getName().equals("Laptop"))
	    .map(d ->d.getName())
	    .collect(
	    	Collectors.toList());
	    for (String d : names) {
			System.out.println(d);
		}
	    long count = result.stream()
	    	.filter(d -> d.getManufacturerId() == 3)
	    	.count();
	    System.out.println("Number of devices where manufacturer ID = 3: " + count);
	    List <Device> deviceColor1 = result.stream()
	    	.filter(d -> d.getColorId() == 1)
	    	.collect(Collectors.toList());
	    for (Device d : deviceColor1) {
			System.out.println("ID: "+ d.getDeviceId() + " device's name: " 
					+ d.getName() + " Description " + d.getDescription()  
					+ " Manufacturer Id: " + d.getManufacturerId()  
					+ " Color Id: " + d.getColorId() 
					+ " Comments: " + d.getComments());
		}
	    Map<Integer, List<Device>> mapa = result.stream()
	    		.collect(
	    				Collectors.groupingBy(Device::getDeviceId));
	    mapa.forEach((key, value) ->
	    		System.out.println(key + ": " + value.toString()));
    }
}
