import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.softtek.academia.JdbcExample.Color;

public class App {
	public static void main( String[] args )
    {
		List<Device> result = new ArrayList<>();
	
		String SQL_SELECT = "Select * from DEVICE";
		
		DBConnection conn = new DBConnection();
		conn.getConnection();
		PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);
	
	    ResultSet resultSet = preparedStatement.executeQuery();
	
	    while (resultSet.next()) {
	    	int deviceId = resultSet.getLong("deviceId");
	        String name = resultSet.getString("name");
	        String description = resultSet.getString("description");
	        int manufacturerId = resultSet.getLong("manufacturerId");
	        int colorId = resultSet.getLong("colorId");
	        String comments = resultSet.getString("comments");
	        Device device = new Device(deviceId, name, description, manufacturerId, 
	        		colorId, comments);
	        result.add(device);
	    }
	    for (Device d : result) {
			System.out.println("ID: "+ d.getDeviceId() + " device's name: " 
					+ d.getName() + " Description " + d.getDescription()  
					+ "Manufacturer Id: " + d.getManufacturerId()  
					+ " Color Id: " + getColorId() 
					+ " Comments: " + d.getComments());
		}
    }
}
