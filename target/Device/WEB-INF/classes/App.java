import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.softtek.academia.JdbcExample.Color;

public class App {
	String SQL_SELECT = "Select * from DEVICE";
	
	DBConnection conn = new DBConnection();
	conn.getConnection();
	PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);

    ResultSet resultSet = preparedStatement.executeQuery();

    while (resultSet.next()) {
        long id = resultSet.getLong("COLORID");
        String name = resultSet.getString("NAME");
        String hexValue = resultSet.getString("HEXVALUE");
        Color color = new Color(id, name, hexValue);
        result.add(color);
}
