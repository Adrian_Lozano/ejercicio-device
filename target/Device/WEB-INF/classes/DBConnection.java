import java.sql.*; 
public class DBConnection {
	private static Connection conn = null;
	private DBConnection() {
		String url ="jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#";
		String driver = "com.mysql.jdbc.Driver";
		String user = "root";
		String pass =  "1234";
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, user, pass);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection() {
		if(conn == null) {
			new DBConnection();
		}
		return conn;
	}
}
